# Open Mod Generator (for openmw)

## Generate and install mod templates.

## Features:

### Auto-Install generated mod template

![](video/Modgen_InstallTest_render.mp4){height=75%, width=75%}


### Generate empty addon-file and project Readme

![](video/Modgen_addonFile_render.mp4){height=75%, width=75%}


### Generate empty folders for asset replacement mods

![](video/Modgen_assetTest_render.mp4){height=75%, width=75%}


### Generate engine handler stubs from dropdown

![](video/Modgen_EngineHandlers_render.mp4){height=75%, width=75%}


### Generate mod interface function call examples from mods that share a parent directory

![](video/Modgen_Interface_render.mp4){height=75%, width=75%}


## Known Bugs:
None

## License
--TODO: say how it is licensed.

