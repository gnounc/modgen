//
// Created by gnounc on 10/14/23.
//

// You may need to build the project (run Qt uic code generator) to get "ui_modGen.h" resolved

//TODO: minor bug - something is setting modgen as its parent, resulting in a "1" in the topleft of the app.

//TODO: bug - first engine handler is skipped if save is hit after a tab is removed.

//TODO: add addEvents section
//TODO: add custom events folder
//TODO: add dump folders button
//TODO: read from dumped folders if they exist, and embedded ones if they dont
#include "modgen.h"
#include "ui_modgen.h"
#include "newtabpopup.h"
#include "addremovedropdown.h"
#include "scriptpage.h"
#include "fileHandler.h"
#include <QPushButton>
#include <QScrollArea>
#include <QUrl>
#include <QDesktopServices>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QComboBox>


modgen::modgen(QWidget *parent) : QTreeWidget(parent), ui(new Ui::modgen) {
	ui->setupUi(this);

	fileHandler::populateInterfaceFunctions();

	tabs = findChild<QTabWidget *>("tabWidget");
	tabs->setCurrentIndex(0);
	tabs->setTabsClosable(true);
	connect(tabs->tabBar(), &QTabBar::tabCloseRequested, tabs->tabBar(), &QTabBar::removeTab);


	tabPage("Files");
	tabPage("Player");
	tabPage("Global");

	tabs->tabBar()->setTabButton(0, QTabBar::RightSide, nullptr);
	tabs->tabBar()->setTabButton(1, QTabBar::RightSide, nullptr);
	tabs->tabBar()->setTabButton(2, QTabBar::RightSide, nullptr);


	//add newtab button
	QPushButton *button_addtab = new QPushButton("+");
	button_addtab->setFixedSize(32, 32);
	connect(button_addtab, SIGNAL(clicked()), this, SLOT(addNewTabPopup()));
	tabs->setCornerWidget(button_addtab, Qt::TopRightCorner);


	input_filename = findChild<QLineEdit *>("input_filename");

	button_lua = findChild<QPushButton *>("button_lua");
	button_save = findChild<QPushButton *>("button_save");
}

void modgen::tabPage(QString pageType) {
	tabs->insertTab(tabs->count(),new scriptpage(this, pageType), pageType);
}

void modgen::addNewTabPopup() {
	newtabpopup* popup = new newtabpopup(this);
}

modgen::~modgen() {
	delete ui;
}

void modgen::showLuaDocs() {
	QUrl url = QUrl("https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_types.html##(types)");
	QDesktopServices::openUrl(url);
}

void modgen::save() {
	if(input_filename->text().isNull() || input_filename->text().isEmpty()) {
		qDebug() << "Filename not set. Cannot generate files.";
		return;
	}

	QString modname = input_filename->text();

	QString cwd = QDir::currentPath() + "/";
	QString parentpath = cwd + "openmw_" + modname + "/";
	QString modpath = parentpath + modname;
	QDir parentFolder = QDir(parentpath);
	QDir modFolder = QDir(modpath);


	if(parentFolder.exists()) {
		qDebug() << "Mod folder already exists. Cannot create folder.";
		return;
	}

	parentFolder.mkdir(parentpath);
	modFolder.mkdir(modpath);
	modFolder.mkdir(modpath + "/scripts/");
	modFolder.mkdir(modpath + "/scripts/" + modname);


	if(scriptpage::getIsAddReadme()) {
		QFile readme = QFile(":/data/templates/README.md");
		readme.copy(parentpath + "/README.md");
	}

	if(scriptpage::getIsAddAddon()) {
		QFile readme = QFile(":/data/templates/template.omwaddon");
		readme.copy(parentpath + "/" + modname + "/" + modname + ".omwaddon");
	}


	//grab all template files and prep them for manipulation
	QString template_script = fileHandler::getTemplate(":/data/templates/script.txt");
	QString global_import_section = fileHandler::getTemplate(":/data/templates/section_imports_global.txt");
	QString player_import_section = fileHandler::getTemplate(":/data/templates/section_imports_player.txt");
	QString local_import_section = fileHandler::getTemplate(":/data/templates/section_imports_local.txt");
//	QString section_variables = getTemplate(":/templates/script.txt");
	QString section_variables = ""; //TODO: placeholder
	QString debug_section = fileHandler::getTemplate(":/data/templates/section_debug.txt");
	QString player_function_section = fileHandler::getTemplate(":/data/templates/section_player_functions.txt");  //TODO: unimplemented
	QString interface_section = fileHandler::getTemplate(":/data/templates/section_interface.txt");  //TODO: unimplemented
	QString global_events_section = fileHandler::getTemplate(":/data/templates/section_global_events.txt");  //TODO: unimplemented
	QString player_events_section = fileHandler::getTemplate(":/data/templates/section_player_events.txt");  //TODO: unimplemented
	QString handlers_section = fileHandler::getTemplate(":/data/templates/section_handlers.txt");


	QString omwscript = "";

	for(int tabIdx = 0; tabIdx < tabs->count(); tabIdx++) {
		QString scriptType = tabs->tabText(tabIdx);
		if(!scriptType.startsWith("Files")) {
			omwscript = omwscript + scriptType.toUpper() + ":scripts/" + modname + "/" + scriptType + ".lua\n";
		}

		auto tab = tabs->widget(tabIdx);
		scriptpage *scriptP = dynamic_cast<scriptpage*>(tab);


		QString import_section = "";    //update in loop, depending on script type;
		QString events_section = "";
		QString function_section = "";

		QString handlers = "";
		QString handlerReturns = "";
		QString handlerTemplate = "";


		foreach(QString handlerName, scriptP->getUsedOptions()) {
			//all scripts that arent global or player, are local/custom scripts
			QString script_type = scriptType.toLower();

			if (script_type.startsWith("files")) {
				modFolder.mkdir(modpath + "/" + handlerName);
				continue;
			}

			if (!script_type.startsWith("player") && (!script_type.startsWith("global"))) {
				script_type = "local";
			}

			QString handlerFilename = ":/data/templates/EngineHandlers/" + script_type + "/" + handlerName + ".txt";
			QFile f_handlerTemplate = QFile(handlerFilename);
			qDebug() << handlerFilename;
			if (f_handlerTemplate.exists()) {
				f_handlerTemplate.open(QIODevice::ReadOnly);
				handlerTemplate = QString(f_handlerTemplate.readAll());
				f_handlerTemplate.close();
			} else {
				qDebug() << script_type + " template did not exist. trying <any>.";

				//this block looks redundant.. but QFile wouldnt let me reuse f_handlerTemplate variable.
				handlerFilename = ":/data/templates/EngineHandlers/any/" + handlerName + ".txt";
				QFile f_handlerTemplate2 = QFile(handlerFilename);
				qDebug() << handlerFilename;
				if (f_handlerTemplate2.exists()) {
					f_handlerTemplate2.open(QIODevice::ReadOnly);
					handlerTemplate = QString(f_handlerTemplate2.readAll());
					f_handlerTemplate2.close();
				} else {
					qDebug() << scriptType + " template did not exist. skipping.";
					continue;
				}
			}

			if (!handlerTemplate.isEmpty()) {
				handlers = handlers + handlerTemplate + "\n";
			}

			handlerReturns = handlerReturns + "\n\t\t" + handlerName + " = " + handlerName + ",";
		}


		QString ifaceFunctions = "";
		foreach(QString interface, scriptP->getInterfaces()) {
			if(interface.length() > 0) {
				qDebug() << interface + " was not empty.";
				ifaceFunctions = ifaceFunctions + "\t--interfaces." + interface + "\n";
			}
		}

		//stomp over functions section so it doesnt show up, if there are no functions
		if(ifaceFunctions.length() > 0) {
			function_section = player_function_section.arg(ifaceFunctions);
		}

		if(scriptType.startsWith("Files")) {
			continue;
		}else if(scriptType == "Global") {
			import_section = global_import_section;
			events_section = global_events_section;
		} else if(scriptType == "Player") {
			import_section = player_import_section;
			events_section = player_events_section;
		} else {
			import_section = local_import_section;
		}

		//take trailing comma off handlers list.
		handlerReturns.chop(1);

		QString final_handlers = handlers_section.arg(interface_section, events_section, handlerReturns);
		QString data = template_script.arg(import_section, section_variables, debug_section, function_section, handlers, final_handlers);

		QString scriptLuaPath = modpath + "/scripts/" + modname + "/" + scriptType + ".lua";
		fileHandler::writeFile(scriptLuaPath, data);
	}

	//write omwscripts file
	fileHandler::writeFile(modpath + "/" + modname + ".omwscripts", omwscript);

	//write to config file, to install mod
	QString dataString = "data=\"" + modpath + "\"\n";
	QString addonString = "";
	QString contentString = "content=" + modname + ".omwscripts\n";
	if(scriptpage::getIsAddAddon()) {
		addonString = "content=" + modname + ".omwaddon\n";
	}

	fileHandler::writeConfig(fileHandler::findConfig(), dataString, addonString, contentString);
	QDesktopServices::openUrl(QUrl(modpath));
}

