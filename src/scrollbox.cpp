//
// Created by gnounc on 12/4/23.
//

#include "scrollbox.h"

scrollbox::scrollbox(QWidget *parent) : QWidget(parent) {
	body = new QVBoxLayout();

	QSpacerItem *vSpacer = new QSpacerItem(1,150, QSizePolicy::Fixed, QSizePolicy::Expanding);
	body->addSpacerItem(vSpacer);
	setLayout(body);
}

scrollbox::~scrollbox() {
}

void scrollbox::addWidget(QWidget *widget) {
//	body->addWidget(widget);
	insertWidget(body->count() -1, widget);
}

void scrollbox::insertWidget(int idx, QWidget *widget) {
	body->insertWidget(idx, widget);
}