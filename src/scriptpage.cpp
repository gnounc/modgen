//
// Created by gnounc on 11/17/23.
//

#include <QDebug>
#include <QWidget>
#include <QHBoxLayout>
#include <QDirIterator>
#include <QCheckBox>
#include "scriptpage.h"
#include "addremovedropdown.h"
#include "doubledropdown.h"
#include "fileHandler.h"

bool scriptpage::isAddReadme;
bool scriptpage::isAddAddon;

scriptpage::scriptpage(QWidget *parent, QString pageType) : QWidget(parent) {
	dropdowns = QVector<addremovedropdown*>();

	scriptType = pageType;
	qDebug() << "scriptpage pageType";
	qDebug() << scriptType;


	//used to set local/global/player on doubledropdown to filter empty interfaces
	//TODO: this bit of code is in like 4 places. so something can probably be redesigned to clean it up
	QString ifaceType = scriptType.toUpper();
	if(ifaceType.contains("GLOBAL") || ifaceType.contains("PLAYER")) {
		//pass;
	} else {
		ifaceType = "LOCAL";
	}

	layout_content = new QVBoxLayout(this);

	if(scriptType.contains("File")) {
		QCheckBox* cb_readme = new QCheckBox("add readme");
		connect(cb_readme, SIGNAL(clicked()), this, SLOT(toggleIsAddReadme()));
		layout_content->addWidget(cb_readme);

		QCheckBox* cb_addAddon = new QCheckBox("add .omwaddons file");
		connect(cb_addAddon, SIGNAL(clicked()), this, SLOT(toggleIsAddAddon()));
		layout_content->addWidget(cb_addAddon);

		dataSource << "fonts" << "icons" << "meshes" << "music" << "sounds" << "splashes" << "textures" << "videos";
		mainDropdown = new addremovedropdown(this, "--add asset folder--", dataSource);
		layout_content->addWidget(mainDropdown);
		dropdowns.append(mainDropdown);

	} else {
		dataSource << fileHandler::getScriptFileList(scriptType);
		mainDropdown = new addremovedropdown(this, "--add engine handler--", dataSource);
		layout_content->addWidget(mainDropdown);
		dropdowns.append(mainDropdown);

		interface_dropdown = new doubledropdown(this, scriptType,"--lists--", fileHandler::getInterfaceScripts(ifaceType));
		layout_content->addWidget(interface_dropdown);
	}
}

QStringList scriptpage::getUsedOptions() {
	qDebug() << mainDropdown->getUsedOptions();
	return mainDropdown->getUsedOptions();
}

QStringList scriptpage::getInterfaces() {
	if(interface_dropdown != nullptr) {
		return interface_dropdown->getInterfaces();
	}
	return QStringList{};
}

void scriptpage::toggleIsAddReadme() {
	QCheckBox *cb = dynamic_cast<QCheckBox*>(sender());
	isAddReadme = cb->isChecked();
}

void scriptpage::toggleIsAddAddon() {
	QCheckBox *cb = dynamic_cast<QCheckBox*>(sender());
	isAddAddon = cb->isChecked();
}

bool scriptpage::getIsAddReadme() {
	return isAddReadme;
}

bool scriptpage::getIsAddAddon() {
	return isAddAddon;
}

//TODO: add destructor to scriptpage,
//TODO: add property delete on close
//TODO: between these two, i suspect the engine handler bug will be fixed.