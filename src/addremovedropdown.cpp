//
// Created by gnounc on 11/17/23.
//

// You may need to build the project (run Qt uic code generator) to get "ui_addRemoveDropdown.h" resolved



#include <QSet>
#include "addremovedropdown.h"
#include "newtabpopup.h"
#include "scrollbox.h"
#include <QDebug>
#include <QScrollArea>
#include <QTimer>

/*
 * addremoveevents widget can use a hashmap for usedOptions
 * QHash<QString scriptname, QStringList events>
 *  [<] label_scriptname] [>]
 *  [dropdown_events >]
 */

addremovedropdown::addremovedropdown(QWidget *parent, QString placeholder_text, QStringList data_source) : QWidget(parent) {
	body = new QVBoxLayout();
	sbox = new scrollbox(this);

	dataSource = data_source;
	placeholderText = placeholder_text;

	terminalDropdown = new dropdown(this, placeholder_text);
	terminalDropdown->setTerminal();
	terminalDropdown->addItems(data_source);

	body->addWidget(terminalDropdown);

	scrollArea = new QScrollArea();
	scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidget(sbox);
	scrollArea->setWidgetResizable(true);
	body->addWidget(scrollArea);

	setLayout(body);
}
/*
	if(separatorIdx > 0) {
		engineHandlerDropdown->insertSeparator(separatorIdx);
	}
*/

addremovedropdown::~addremovedropdown() {
};

QStringList addremovedropdown::getOptions() {
	QStringList options;

	foreach(dropdown* dd, dropdowns) {
		options << dd->getText();
	}
	return options;
}


QStringList addremovedropdown::getUsedOptions() {
	return usedOptions;
}

//TODO: maybe make dropdowns readonly after new dropdown is added
void addremovedropdown::addRemove() {
	QPushButton *button = dynamic_cast<QPushButton*>(sender());

	if(button->text() == "+") {
		//if the dropdown isnt set, dont add new dropdown.
		if(terminalDropdown->currentIndex() < 0) {
			return;
		}

		QString menuOption = terminalDropdown->getText();
		usedOptions.append(menuOption);
		updateTerminalDropdown();

		dropdown *dd = new dropdown(this, menuOption);
		dd->addItems(QStringList{menuOption});
		dd->setCurrentIndex(0);

		sbox->addWidget(dd);
		dropdowns.append(dd);

		//add delay so scroll widget can update its size calcs, *then* attempt to scroll down
        QTimer::singleShot(5, this, SLOT(scrollDown()));
	} else {
		dropdown *dd = dynamic_cast<dropdown*>(button->parent());
		usedOptions.removeOne(dd->getText());
		updateTerminalDropdown();

		dropdowns.removeOne(dd);
		dd->~dropdown();
		dd = nullptr;
	}
}

void addremovedropdown::updateTerminalDropdown() {
	QStringList unusedValues = (QSet<QString>(dataSource.begin(), dataSource.end()) - QSet<QString>(usedOptions.begin(), usedOptions.end())).values();
	terminalDropdown->clear();
	terminalDropdown->addItems(unusedValues);
	terminalDropdown->setCurrentIndex(-1);
}

void addremovedropdown::setData(QStringList data_source) {
	dataSource = data_source;
}

void addremovedropdown::scrollDown() {
	scrollArea->ensureWidgetVisible(dropdowns.takeLast());
}
