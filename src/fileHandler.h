//
// Created by gnounc on 11/28/23.
//

#ifndef MODGEN_FILEHANDLER_H
#define MODGEN_FILEHANDLER_H


#include <QString>
#include <QDir>

class fileHandler {

	public:
		enum filepath{Basename, Fullname, Fullpath};
		static void writeFile(QString path, QString data);
		static QString findConfig();
		static void writeConfig(QString configPath, QString dataString, QString addonString, QString contentString);
		static QString getTemplate(QString path);
		static QStringList getScriptFileList(QString scriptType);
		static QStringList getFilesInPath(QString path, QDir::Filter filter, QStringList fileTypes, filepath = Basename);
		static void populateInterfaceFunctions();
		static QStringList getInterfaceFunctions(QString scriptType, QString interfaceName);
		static QStringList getInterfaceScripts(QString scriptType);


	private:
		static QMap<QString, QMap<QString, QStringList>> interfaceFunctions;
		static QMap<QString, QStringList> interfaces;
		static QStringList findInterfacesInFile(QString(filename));


};


#endif //MODGEN_FILEHANDLER_H
