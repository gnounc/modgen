//
// Created by gnounc on 11/16/23.
//

#include "newtabpopup.h"
#include "ui_newtabpopup.h"
#include "modgen.h"


bool newtabpopup::isOpen;

newtabpopup::newtabpopup(QWidget *parent) : QDialog(parent), ui(new Ui::newtabpopup) {
    setAttribute(Qt::WA_DeleteOnClose);

	if(isOpen) {
		return;
	}

	ui->setupUi(this);

	layout_content = findChild<QVBoxLayout *>("layout_content");

	button_ok = findChild<QPushButton *>("button_ok");
	button_cancel = findChild<QPushButton *>("button_cancel");

	QStringList scriptTypes;
	scriptTypes << "Custom" << "Activator" << "Armor" << "Book" << "Clothing" << "Container" << "Creature" << "Door" << "Ingredient" << "Light" << "Misc_Item" << "Npc" << "Potion" << "Weapon" << "Apparatus" << "Lockpick" << "Probe" << "Repair";
	scriptDropdown = new QComboBox(this);
	scriptDropdown->addItems(scriptTypes);

	layout_content->addWidget(scriptDropdown);

	connect(button_ok, SIGNAL(clicked()), this, SLOT(cb_confirm()));
	connect(button_cancel, SIGNAL(clicked()), this, SLOT(close()));

	show();

	isOpen = true;
}

void newtabpopup::cb_confirm() {

	modgen *mg = dynamic_cast<modgen*>(parent());
	QString scriptType = scriptDropdown->currentText();

	if(scriptType.isEmpty()) {
		return;
	}

	mg->tabPage(scriptType);
	mg->tabs->setCurrentIndex(mg->tabs->count() -1);
	close();
}

newtabpopup::~newtabpopup() {
	isOpen = false;
	delete ui;
}
