//
// Created by gnounc on 11/16/23.
//

#ifndef MODGEN_NEWTABPOPUP_H
#define MODGEN_NEWTABPOPUP_H

#include <QWidget>
#include <QDialog>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QComboBox>

QT_BEGIN_NAMESPACE
namespace Ui { class newtabpopup; }
QT_END_NAMESPACE

class newtabpopup : public QDialog {
Q_OBJECT

public:
	explicit newtabpopup(QWidget *parent = nullptr);
	~newtabpopup() override;

signals:

	public slots:
		void cb_confirm();

private:
	Ui::newtabpopup *ui;
	QTabWidget* tabs;
	QVBoxLayout * layout_content;
	QPushButton* button_ok;
	QPushButton* button_cancel;
	QComboBox* scriptDropdown;
	QComboBox* subscriptDropdown;

	static bool isOpen;
};


#endif //MODGEN_NEWTABPOPUP_H
