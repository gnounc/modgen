//
// Created by gnounc on 12/4/23.
//

#ifndef MODGEN_SCROLLBOX_H
#define MODGEN_SCROLLBOX_H


#include <QWidget>
#include <QVBoxLayout>

class scrollbox : public QWidget {
Q_OBJECT

	public:
		explicit scrollbox(QWidget *parent);
		~scrollbox() override;
		void addWidget(QWidget *widget);
		void insertWidget(int idx, QWidget *widget);

public  slots:

		signals:


	private:
		QVBoxLayout *body;

};


#endif //MODGEN_SCROLLBOX_H
