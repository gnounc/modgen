local function onLoad()
	settings = playerStorage:asTable()["settings"]
end

local function onSave()
	playerStorage:set("settings", settings)
end 
