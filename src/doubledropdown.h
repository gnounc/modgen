//
// Created by gnounc on 11/27/23.
//

#ifndef MODGEN_DOUBLEDROPDOWN_H
#define MODGEN_DOUBLEDROPDOWN_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>
#include "dropdown.h"
#include "addremovedropdown.h"

class doubledropdown : public QWidget {
Q_OBJECT

	public:
		doubledropdown(QWidget *parent, QString script_type, QString placeholder_text, QStringList data_source);

		QString scriptType;
		QVBoxLayout *body;
		QComboBox *dropdown_scripts;
		addremovedropdown *dropdown_interfaces;
		QVector<dropdown*> dropdowns;

		QStringList scripts;
		QString placeholderText;
		QStringList dataSource;
		QStringList usedOptions;
		QStringList getInterfaces();

		QPair<QString, QString> getText();

	public slots:
		void addRemove();
		void setScriptSelections();

	private:

};


#endif //MODGEN_DOUBLEDROPDOWN_H
