//
// Created by gnounc on 11/17/23.
//

#ifndef MODGEN_ADDREMOVEDROPDOWN_H
#define MODGEN_ADDREMOVEDROPDOWN_H

#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QVBoxLayout>
#include <QScrollArea>
#include "dropdown.h"
#include "scrollbox.h"


class addremovedropdown : public QWidget {
Q_OBJECT

public:
	explicit addremovedropdown(QWidget *parent, QString placeholder_text, QStringList data_source);
	~addremovedropdown() override;
	QString placeholderText;
	QStringList getOptions();
	void updateTerminalDropdown();
	void setData(QStringList data_source);
	QStringList getUsedOptions();

private:
	QVBoxLayout *body;
	scrollbox *sbox;
	QVector<dropdown*> dropdowns;
	dropdown *terminalDropdown;

	QScrollArea *scrollArea;

	QStringList dataSource;
	QStringList usedOptions;


private slots:
	void addRemove();
	void scrollDown();

};



#endif //MODGEN_ADDREMOVEDROPDOWN_H



