//
// Created by gnounc on 11/28/23.
//

#include <QDebug>
#include <QFile>
#include <QDir>
#include <QDirIterator>
#include <QRegularExpression>
#include "fileHandler.h"


QMap<QString, QMap<QString, QStringList>>fileHandler::interfaceFunctions;

void fileHandler::writeFile(QString path, QString data) {
	qDebug() << "writing file: " + path;

	QFile *file = new QFile(path);
	if (file->open(QFile::Append)) {
		file->write(data.toUtf8());
		file->flush();
		file->close();
	}
}

QString fileHandler::findConfig() {
	qDebug() << "locating config file.";

	QString config;
	QString os = QSysInfo::kernelType().toLower();

	QString home = QDir::homePath();

	if(os == "windows") {
		config = home + QString("/Documents/My Games/OpenMw/openmw.cfg");
	}
	if(os == "darwin") {
		config = home + QString("/Library/Preferences/openmw/openmw.cfg");
	}
	if(os == "linux") {
		config = home + QString("/.config/openmw/openmw.cfg");
	}

	return config;
}

void fileHandler::writeConfig(QString configPath, QString dataString, QString addonString, QString contentString) {
	qDebug() << "writing to config file: " + configPath;
	qDebug() << "\n";

	//skip if already installed
	QFile *configFileRead = new QFile(configPath);
	configFileRead->open(QIODevice::ReadOnly);
	QString cfgString = configFileRead->readAll();
	configFileRead->close();

	if(cfgString.contains((dataString))) {
		return;
	}

	if(cfgString.contains((addonString))) {
		return;
	}

	if(cfgString.contains((contentString))) {
		return;
	}

	QFile *configFileWrite = new QFile(configPath);
	configFileWrite->open(QIODevice::Append);
	configFileWrite->write("\n");

	configFileWrite->write(dataString.toUtf8());
	configFileWrite->write(addonString.toUtf8());
	configFileWrite->write(contentString.toUtf8());

	configFileWrite->close();
}


QString fileHandler::getTemplate(QString path) {
	QFile f_script = QFile(path);
	f_script.open(QIODevice::ReadOnly);

	QString template_script = QString(f_script.readAll());
	f_script.close();

	return template_script;
}



//TODO: refactor this to just getFilesInPath, and pass in the engine handler path.
//TODO: *then* use getFilesInPath in populateInterfaceFunctions
//QStringList fileHandler::getScriptList(QString scriptType) {
QStringList fileHandler::getFilesInPath(QString path, QDir::Filter filter, QStringList fileTypes, filepath pathType) {

	QStringList fileList;
	QDirIterator it(path, fileTypes, filter, QDirIterator::Subdirectories);
	while (it.hasNext()) {
		auto item = QFileInfo(it.next());

		if (item.isFile()) {
			switch(pathType) {
				case Basename:
				    fileList << item.baseName();
					break;
				case Fullname:
					fileList << item.fileName();
					break;
				case Fullpath:
				fileList << item.absoluteFilePath();
			}
		}
	}

	return fileList;
}

QStringList fileHandler::getScriptFileList(QString scriptType) {

	//add files from "<any>" folder
	QStringList fileList;
	fileList << getFilesInPath(":/data/templates/EngineHandlers/any", QDir::Files,QStringList{"*.txt"});

	QString pageType = scriptType.toLower();
	if((!pageType.contains("player")) && (!pageType.contains("global"))) {
		pageType = "local";
	}

	//add files from "<player|global|local>" folder
	fileList << getFilesInPath(":/data/templates/EngineHandlers/" + pageType, QDir::Files, QStringList{"*.txt"});

	return fileList;
}

QStringList fileHandler::findInterfacesInFile(QString(filename)) {
	qDebug() << " ";
	qDebug() << "finding interfaces";

	QStringList functions;
	if(!QFile::exists(filename)) {
		qDebug() << "interface file did not exist.";
		return functions;
	}

	QFile file = QFile(filename);
	file.open(QFile::ReadOnly);
	QString script = file.readAll();
	file.close();

	QRegularExpression iFacepattern("interfaceName.*\"(.*)\"");

	if(!iFacepattern.match(script).hasMatch()) {
		qDebug() << "no interfaces found.";
		return functions;
	}

	QString interfaceName = iFacepattern.match(script).capturedTexts()[1];
	qDebug() << " ";
	QString divider = QString().fill('-', interfaceName.count());
	qDebug() << interfaceName;
	qDebug() << divider;

	QRegularExpression iFuncpattern("interface.*{([^}]+)");
	QStringList iFaceMatches = iFuncpattern.match(script).capturedTexts();
	if(iFaceMatches.isEmpty()) {
		qDebug() << "  iFaceMatches was empty.";
		return functions;
	}

	QString iFaceFunctions = iFaceMatches[1];
	QStringList foundFunctions = iFaceFunctions.split(",\n");

	if(foundFunctions.isEmpty()) {
		return functions;
	}

	//pull function names out of interface return block
	foreach(QString capture, foundFunctions) {
		if(capture.toLower().trimmed().startsWith("version")) {
			continue;
		}

		//fix for trailing comma : (
		if(!capture.contains("=")) {
			continue;
		}

		QString func = capture.split("=")[1];
		functions << func.trimmed();
	}


	QStringList retFunctions;
	foreach(QString iFunc, functions) {
		QString rgex = "local function (" +iFunc + ".*)";
		QRegularExpression funcSig(rgex);

		QStringList funcSigMatches = funcSig.match(script).capturedTexts();
		if(funcSigMatches.isEmpty()) {
			qDebug() << "  funcSigMatches for " + iFunc + " was empty.";
			continue;
		}

		qDebug() << funcSigMatches[1].trimmed();
		retFunctions << interfaceName + "." + funcSigMatches[1].trimmed();
	}

	return retFunctions;
}


//TODO: general cleanup:
//TODO:     simplify code to reduce temp variables, rename variables to be more informative
//TODO:     remove debugging qDebugs() and update console prints to be more informative
void fileHandler::populateInterfaceFunctions() {
	qDebug() << " ";
	qDebug() << "--populating interface functions--";
	QString cwd = QDir::currentPath() + "/";

	QStringList folders;
    QDirIterator it(cwd, QDir::Dirs | QDir::NoSymLinks | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
	while (it.hasNext()) {
		auto item = QFileInfo(it.next());

		if (item.isDir()) {
		    folders << item.absoluteFilePath();
		}
	}

	//get lines from *.omwscripts file so we can look for NPC/PLAYER/GLOBAL prefixes
	foreach(auto folder, folders) {
		QStringList scriptPrefixes = QStringList{};
		QStringList omwscripts = getFilesInPath(folder, QDir::Files, QStringList{"*.omwscripts"}, Fullpath);
		foreach(auto omws, omwscripts) {
			QFile f = QFile(omws);
			f.open(QIODevice::ReadOnly);
			QString temp = f.readAll();
			scriptPrefixes = temp.split("\n");
			f.close();
		}

		QStringList interfaceList;
		QStringList files = getFilesInPath(folder, QDir::Files, QStringList{"*.lua"}, Fullpath);
		foreach(QString filename, files) {
			qDebug() << " ";
			qDebug() << "    filename";

			qDebug() << "    " + filename;
			interfaceList = findInterfacesInFile(filename);

			//no interface? no work.
			if(interfaceList.count() < 1) {
				qDebug() << " ";
				qDebug() << "    interfaceList.count < 1, skipping";
				continue;
			}

			qDebug() << " ";
			qDebug() << "---interface list---";
			qDebug() << interfaceList;

			QString scriptInterface = interfaceList[0];
			QString interfaceName = scriptInterface.split(".")[0];
			qDebug() << " ";
			qDebug() << "    interfaceName";
			qDebug() << "    " + interfaceName;

			QFileInfo fileInfo(filename);
			QString scriptFilename = fileInfo.baseName() + ".lua";
			qDebug() << "    scriptFilename";
			qDebug() << "    " + scriptFilename;

			QString prefix = "";
			QStringList prefixLines = scriptPrefixes.filter(scriptFilename);
			qDebug() << "    ----prefixLines begin ---";
			qDebug() << prefixLines;
			qDebug() << "---end----";


			if(prefixLines.count() > 0) {
				QString prefixLine = prefixLines[0];
				qDebug() << "    prefixLine";
				qDebug() << "    " + prefixLine;
				qDebug() << prefixLine.split(":")[0];
				prefix = prefixLine.split(":")[0];
			} else {
				prefix = "LOCAL";
			}
			qDebug() << "    prefix";
			qDebug() << "    " + prefix;

			QMap<QString, QStringList> iFaceMap = QMap<QString, QStringList>();
			if(interfaceFunctions.contains(prefix)) {
				iFaceMap = interfaceFunctions[prefix];
			}
			qDebug() << prefix;
			iFaceMap.insert(interfaceName, interfaceList);
			interfaceFunctions.insert(prefix, iFaceMap);
			qDebug() << "!new interfaceFunctions!+";
			qDebug() << interfaceFunctions;
		}
	}
}

QStringList fileHandler::getInterfaceScripts(QString scriptType) {
	return interfaceFunctions[scriptType].keys();
}

QStringList fileHandler::getInterfaceFunctions(QString scriptType, QString interfaceName) {
	return interfaceFunctions[scriptType][interfaceName];
}
