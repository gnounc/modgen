#include <QApplication>
#include <QPushButton>
#include "modgen.h"

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);

	modgen form = modgen();
	form.setFixedSize(600, 650);
	form.setWindowTitle("openModGen");
	form.show();

	return QApplication::exec();
}
