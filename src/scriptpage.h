//
// Created by gnounc on 11/17/23.
//

#ifndef MODGEN_SCRIPTPAGE_H
#define MODGEN_SCRIPTPAGE_H


#include "addremovedropdown.h"
#include "doubledropdown.h"

class scriptpage : public QWidget {
Q_OBJECT

	public:
		explicit scriptpage(QWidget *parent = nullptr, QString scriptType = "global");
		QStringList getUsedOptions();

		static bool getIsAddReadme();
		static bool getIsAddAddon();
		QStringList getInterfaces();


	public slots:
		void toggleIsAddReadme();
		void toggleIsAddAddon();

	private:
		QStringList dataSource;
		QStringList usedDataSource;

		addremovedropdown* mainDropdown;

		QString scriptType;
		QVBoxLayout* layout_content;
		doubledropdown *interface_dropdown = nullptr;
		QVector<addremovedropdown*> dropdowns;
		int separatorIdx;

		static bool isAddReadme;
		static bool isAddAddon;

};


#endif //MODGEN_SCRIPTPAGE_H
