//
// Created by gnounc on 11/26/23.
//



#include "dropdown.h"
#include <QDebug>

dropdown::dropdown(QWidget *parent, QString text) : QWidget(parent) {

	ddown = new QComboBox();
	ddown->setPlaceholderText(text);
	connect(ddown, SIGNAL(currentIndexChanged(int)), this, SLOT(selectionChanged(int)));

	body = new QHBoxLayout();
	body->addWidget(ddown);

	button_add = new QPushButton("-");
	button_add->setFixedSize(32, 32);
	button_add->setParent(this);


	connect(button_add, SIGNAL(clicked()), parent, SLOT(addRemove()));
	body->addWidget(button_add);
	setLayout(body);
}

dropdown::~dropdown() {
}

void dropdown::clear() {
	ddown->clear();
}

void dropdown::selectionChanged(int idx) {
	emit currentIndexChanged(idx);
}

void dropdown::setCurrentIndex(int idx) {
	ddown->setCurrentIndex(idx);
}
void dropdown::setTerminal() {
	setButtonText("+");

	QSizePolicy sp_retain = ddown->sizePolicy();
	sp_retain.setRetainSizeWhenHidden(true);
	ddown->setSizePolicy(sp_retain);
//	ddown->setHidden(true);
	ddown->setCurrentIndex(-1);
}

void dropdown::setButtonText(QString text) {
	button_add->setText(text);
}


QString dropdown::getText() {
	return ddown->currentText();
}

void dropdown::addItems(QStringList items) {
	ddown->addItems(items);
}

QString dropdown::placeholderText() {
	return ddown->placeholderText();
}

int dropdown::currentIndex() {
	return ddown->currentIndex();
}