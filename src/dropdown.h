//
// Created by gnounc on 11/26/23.
//

#ifndef MODGEN_DROPDOWN_H
#define MODGEN_DROPDOWN_H



#include <QWidget>
#include <QPushButton>
#include <QComboBox>
#include <QHBoxLayout>

class dropdown : public QWidget {
Q_OBJECT

	public:
		explicit dropdown(QWidget *parent, QString text);
		~dropdown() override;

		QPushButton *button_add;
		void setButtonText(QString text);
		QString getText();
		void addItems(QStringList items);
		void setTerminal();
		void setCurrentIndex(int idx);
		void clear();
		QString placeholderText();
		int currentIndex();


public  slots:
			void selectionChanged(int idx);

		signals:
			void currentIndexChanged(int index);


	private:
		QHBoxLayout *body;
		QComboBox *ddown;

};


#endif //MODGEN_DROPDOWN_H
