//
// Created by gnounc on 10/14/23.
//

#ifndef MODGEN_MODGEN_H
#define MODGEN_MODGEN_H

#include <QWidget>
#include <QTreeWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QLineEdit>


QT_BEGIN_NAMESPACE
namespace Ui { class modgen; }
QT_END_NAMESPACE

class modgen : public QTreeWidget {
Q_OBJECT


public:
	explicit modgen(QWidget *parent = nullptr);

	void tabPage(QString pageType);

	~modgen() override;

	QTabWidget* tabs;

	QLineEdit* input_filename;

	QCheckBox* cb_readme;
	QCheckBox* cb_gitignore;
	QCheckBox* cb_addon;

	QCheckBox* cb_player_debug;
	QCheckBox* cb_player_doOnce;

	QCheckBox* cb_global_debug;
	QCheckBox* cb_global_doOnce;

	QPushButton* button_lua;
	QPushButton* button_save;

	signals:

public slots:
		void addNewTabPopup();
		void showLuaDocs();
		void save();


private:
	Ui::modgen *ui;

	QVector<QCheckBox*> global_handler_checkboxes;
	QVector<QCheckBox*> player_handler_checkboxes;

};


#endif //MODGEN_MODGEN_H
