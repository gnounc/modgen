//
// Created by gnounc on 11/27/23.
//

//TODO: consider changing the dropdowns added to the scrollarea to be labels
//TODO: to try to reduce vertical space

#include "doubledropdown.h"
#include "fileHandler.h"
#include "addremovedropdown.h"
#include <QDebug>

doubledropdown::doubledropdown(QWidget *parent, QString script_type, QString placeholder_text, QStringList data_source) : QWidget(parent) {

	scriptType = script_type.toUpper();
	if(scriptType.contains("GLOBAL") || scriptType.contains("PLAYER")) {
		//pass;
	} else {
		scriptType = "LOCAL";
	}

	dataSource = data_source;
	placeholderText = placeholder_text;
	usedOptions = QStringList();

	body = new QVBoxLayout();

	dropdown_scripts = new QComboBox();
	dropdown_scripts->setPlaceholderText("--select script--");
	dropdown_scripts->addItems(data_source);
	connect(dropdown_scripts, SIGNAL(currentIndexChanged(int)), this, SLOT(setScriptSelections()));

	dropdown_interfaces = new addremovedropdown(this, "--add interface--", QStringList{});

	body->addWidget(dropdown_scripts);
	body->addWidget(dropdown_interfaces);
	setSizePolicy(QSizePolicy::Expanding , QSizePolicy::Expanding );


	setLayout(body);
}

void doubledropdown::addRemove() {
	QPushButton* button = dynamic_cast<QPushButton*>(sender());
	dropdown* parentDropdown = dynamic_cast<dropdown*>(sender()->parent());

	if(button->text() == "+") {
		//dont add new if selection isnt set
		if(parentDropdown->currentIndex() < 0) {
			return;
		}

		setScriptSelections();
	} else {
		dropdown *dd = dynamic_cast<dropdown*>(button->parent());

		usedOptions.removeOne(dd->placeholderText());
		dropdowns.removeOne(dd);
		dd->~dropdown();
		dd = nullptr;
		setScriptSelections();
	}
}


void doubledropdown::setScriptSelections() {
	QString interfaceName = dropdown_scripts->currentText();

	QStringList interfaces = fileHandler::getInterfaceFunctions(scriptType, interfaceName);

	dropdown_interfaces->setData(interfaces);
	dropdown_interfaces->updateTerminalDropdown();
}

QStringList doubledropdown::getInterfaces() {
	return dropdown_interfaces->getUsedOptions();
}